using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ShooterSimple : MonoBehaviour
{
    public float speed;

    public float shootInterval = 2f;

	public Vector2 lowerLeftBound, upperRightBound;

    public float xMinPos, xMaxPos;

    public ProjectileSimple bullet;

    private bool goingRight;

    private void Start()
    {
        InvokeRepeating("Shoot", shootInterval, shootInterval);
    }

    private void Update()
    {
       
            CorrectDirection();
            Move();
     
    }

    private void Move()
    {
        if (goingRight)
        {
            transform.position = transform.position + Vector3.right * speed * Time.deltaTime;
        }
        else
        {
            transform.position = transform.position + Vector3.left * speed * Time.deltaTime;
        }
    }

    public void SwitchDirection()
    {
        goingRight = !goingRight;
    }

    private void CorrectDirection()
    {

        //Si está caminando hacia la derecha y se pasa del x máximo deje de caminar hacia la derecha
        if (goingRight && transform.position.x > xMaxPos)
        {
            goingRight = false;
        }
        //Si no(!) está caminando hacia la derecha y se pasa del x mínimo, camine hacia la derecha
        if (!goingRight && transform.position.x < xMinPos)
        {
            goingRight = true;
        }
    }

	private void Shoot()
	{
		ProjectileSimple copiaBullet = Instantiate(bullet);
		copiaBullet.transform.position = transform.position;
		copiaBullet.transform.parent = transform;
		copiaBullet.upperRightBound = upperRightBound;
		copiaBullet.lowerLeftBound = lowerLeftBound;
	}
}
