using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class UpDown : MonoBehaviour
{
    public float speed;
    public float yMinPos, yMaxPos;
    private bool goingUp;
    
    private void Update()
    {
        CorrectDirection();
        Move();
    }

    private void Move()
    {
        if (goingUp)
        {
            transform.position = transform.position + Vector3.up * speed * Time.deltaTime;
        }
        else
        {
            transform.position = transform.position + Vector3.down * speed * Time.deltaTime;
        }
    }

    public void SwitchDirection()
    {
        goingUp = !goingUp;
    }

    private void CorrectDirection()
    {

        //Si está caminando hacia la derecha y se pasa del x máximo deje de caminar hacia la derecha
        if (goingUp && transform.position.y > yMaxPos)
        {
            goingUp = false;
        }
        //Si no(!) está caminando hacia la derecha y se pasa del x mínimo, camine hacia la derecha
        if (!goingUp && transform.position.y < yMinPos)
        {
            goingUp = true;
        }
    }
}
