using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSimple : MonoBehaviour
{
    public Vector2 lowerLeftBound, upperRightBound;

    public float speed;

    public Vector3 bulletDirection;

    private void Update()
    {
        Move();
        CheckYPosition();
    }

    private void Move()
    {
        transform.position = transform.position + bulletDirection * speed * Time.deltaTime;
    }

    private void CheckYPosition()
    {
		if (transform.localPosition.y > upperRightBound.y || transform.localPosition.y < lowerLeftBound.y ||
			transform.localPosition.x > upperRightBound.x || transform.localPosition.x < lowerLeftBound.x)
        {
            //Si se pasa en y del máximo la destruimos
            Destroy(gameObject);
        }
    }
}