using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSimple : MonoBehaviour
{
    public Vector2 lowerLeftBound, upperRightBound;

    public float speed;

    public Vector3 bulletDirection;

    private void Update()
    {
        Move();
        CheckYPosition();
    }

    private void Move()
    {
        transform.position = transform.position + bulletDirection * speed * Time.deltaTime;
    }

    private void CheckYPosition()
    {
        if (transform.position.y > upperRightBound.y || transform.position.y < lowerLeftBound.y ||
            transform.position.x > upperRightBound.x || transform.position.x < lowerLeftBound.x)
        {
            //Si se pasa en y del máximo la destruimos
            Destroy(gameObject);
        }
    }
}