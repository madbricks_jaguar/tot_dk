﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevelButton : MonoBehaviour
{
	public string SceneName;

	public void LoadScene()

	{
		SceneManager.LoadScene (SceneName);
	}

	private void OnTriggerEnter2D (Collider2D collider)
	{

		LoadScene ();

	}
}
