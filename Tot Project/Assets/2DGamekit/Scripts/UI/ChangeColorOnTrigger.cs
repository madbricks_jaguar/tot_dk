﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnTrigger : MonoBehaviour
{
	public Color change;
	private SpriteRenderer spriterenderer;
	private void Awake ()
	{
		
		spriterenderer = GetComponent<SpriteRenderer> ();

	}
	private void OnTriggerEnter2D (Collider2D collider)
	{

		spriterenderer.color = change;

	}
}
